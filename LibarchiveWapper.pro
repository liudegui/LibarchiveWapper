#-------------------------------------------------
#
# Project created by QtCreator 2017-12-04T13:13:34
#
#-------------------------------------------------

QT       -= core gui

TARGET = archiveWapper
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++11
!win32{
    QMAKE_CXXFLAGS  += -std=c++11
}

SOURCES += \
    src/ArchiveReader.cpp \
    src/ArchiveWriter.cpp \
    src/MemoryWriterCallback.cpp

HEADERS += \
    include/CommonDef.h \
    include/ScopeExit.h \
    include/MemoryWriterCallback.h \
    include/ArchiveWriter.h \
    include/ArchiveReader.h \
    include/LogHelper.h

INCLUDEPATH += $$PWD/include $$PWD/libarchive

unix {
    target.path = /usr/lib
    INSTALLS += target
}
