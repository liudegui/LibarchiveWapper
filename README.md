# LibarchiveWapper

This project forked from [moor](https://github.com/saboorian/moor), but modified a lot by me.

### A C++ wrapper for LibArchive

This project is a wrapper for
[LibArchive](http://libarchive.github.com/)
library.

Supports disk-based and in-memory compression/decompression.

For license information, see the file COPYING.


