
#include "gtest/gtest.h"
#include "gtest/gtest-spi.h"

#include "../ArchiveWriter.h"
#include "../ArchiveReader.h"
#include <list>
#include <vector>
#include <iostream>
#include <fstream>
#include <assert.h>


TEST(LibarchiveWapperTest, testArchiveWriterOpen)
{
	ArchiveWriter compressor(eFormat_ZIP, eCompression_none);
	std::list<unsigned char> lout;
	bool ret = compressor.open(lout);
	ASSERT_TRUE(ret == true);

	ret = compressor.close();
	ASSERT_TRUE(ret == true);

	ret = compressor.close();
	ASSERT_TRUE(ret == true);

	ArchiveWriter compressor1(eFormat_ZIP, eCompression_none);
	ret = compressor1.open("test_compress.zip");
	ASSERT_TRUE(ret == true);
	ret = compressor1.close();
	ASSERT_TRUE(ret == true);

	size_t len = 1024;
	unsigned char buf[1024];
	ArchiveWriter compressor2(eFormat_ZIP, eCompression_none);
	ret = compressor2.open(buf, &len);
	ASSERT_TRUE(ret == true);
	ret = compressor2.close();
	ASSERT_TRUE(ret == true);
}




TEST(LibarchiveWapperTest, testArchiveWriterAdd)
{
    bool ret = false;
    ArchiveWriter compressor(eFormat_ZIP, eCompression_none);
    ret = compressor.open("test_compress.zip");
    ASSERT_TRUE(ret == true);

    std::ofstream ostr("test1");
    ostr << "Hello, world!" << std::endl;
    ostr.close();

    char buff[1024] = {"11111111111"};

    ret = compressor.addFile("test2", (const unsigned char*)buff, 10);
    ASSERT_TRUE(ret == true);

    ret = compressor.addDirectory("C:/Windows/Help/zh-CN");
    ASSERT_TRUE(ret == true);
    compressor.close();
}

TEST(LibarchiveWapperTest, testArchiveWriterCompress)
{
    bool ret = false;

    std::vector<std::string> fileList;
    fileList.push_back("C:/Windows/Help/api.chm");
    fileList.push_back("C:/Windows/Help/zh-CN/credits.rtf");

	ArchiveWriter compressor1("test_compress1.zip", eFormat_ZIP, eCompression_none);
    ret = compressor1.compress("", fileList);
    ASSERT_TRUE(ret == true);
	compressor1.close();

	ArchiveWriter compressor2("test_compress2.zip", eFormat_ZIP, eCompression_none);
    ret = compressor2.compress("C:/Windows/Help", fileList);
    ASSERT_TRUE(ret == true);
	compressor2.close();

	int32_t errCode = compressor2.getLastErrorCode();
	ASSERT_TRUE(errCode == 0);


	ArchiveWriter compressor3("test_compress3.zip", eFormat_ZIP, eCompression_none);
    ret = compressor3.compress("C:/Windows/Help2", fileList);
    ASSERT_TRUE(ret == false);
    compressor3.close();

	errCode = compressor3.getLastErrorCode();
	ASSERT_TRUE(errCode != 0);
}

