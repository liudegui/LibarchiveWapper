
#include "ArchiveWriter.h"
#include "ArchiveReader.h"
#include <list>
#include <vector>
#include <iostream>
#include <fstream>
#include <assert.h>



int main()
{
	std::string dst = "test1.zip";
	ArchiveWriter compressor(dst, eFormat_ZIP, eCompression_none);
	std::vector<std::string> fileList;
    fileList.push_back("Dbgview.exe");
    fileList.push_back("mem_dir/array");

    std::string base = "LibarchiveWapperTest";
    assert(compressor.compress("", fileList));

	ArchiveReader reader1("test1.zip");
    assert(reader1.extractTo("test1"));

	return 0;
}
