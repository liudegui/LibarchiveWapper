/*
 * Copyright (c) 2013 Mohammad Mehdi Saboorian
 *
 * This is part of moor, a wrapper for libarchive
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef COMMON_DEFINE_H
#define COMMON_DEFINE_H

#include <stdint.h>

enum eFormats
{
	eFormat_pax = 0,
	eFormat_tar = 1,
	eFormat_ZIP = 2,
	eFormat_7Zip = 3
};

enum eCompressions
{
	eCompression_none = 0,
	eCompression_gzip = 1,
	eCompression_bzip2 = 2,
	eCompression_lzma = 3
};

enum eFileTypes
{
	eFileType_Regular = 0,
	eFileType_Directory = 1
};

enum eArchiveOperation
{
	eArchiveFailed = -1,
	eArchiveSuccess = 0
};

const char* const FORMAT_COMPRESSION_7ZIP = "compression";
const char* const FORMAT_COMPRESSION_7ZIP_STORE = "store";


enum eArchiveError
{
	eErrorUnknown = 1000,
    eFileNotFound,
    eFileNotValid,
    eEntryFileTypeNotSupported,
    eDirectoryPathError,
	eMaxErrorNUM,
    eSuccess = 0
	/*
	#define	ARCHIVE_EOF	  1			// Found end of archive. 
	#define	ARCHIVE_OK	  0			// Operation was successful. 
	#define	ARCHIVE_RETRY	(-10)	// Retry might succeed. 
	#define	ARCHIVE_WARN	(-20)	// Partial success. 
									// For example, if write_header "fails", then you can't push data. 
	#define	ARCHIVE_FAILED	(-25)	// Current operation cannot complete. 
									// But if write_header is "fatal," then this archive is dead and useless. 
	#define	ARCHIVE_FATAL	(-30)	// No more operations are possible. 
	*/
};

#endif
