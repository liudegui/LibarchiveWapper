/*
 * Copyright (c) 2013 Mohammad Mehdi Saboorian
 *
 * This is part of moor, a wrapper for libarchive
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
#ifndef Archive_Writer_H
#define Archive_Writer_H

#include <string>

#include <iterator>
#include <list>
#include <vector>

#include "CommonDef.h"

struct archive;
struct archive_entry;

class ArchiveWriter
{
public:
	//deprecated
    ArchiveWriter(const std::string& archiveFileName, const eFormats& format, const eCompressions& compression);
	//deprecated
    ArchiveWriter(std::list<unsigned char>& outBuffer, const eFormats& format, const eCompressions& compression);
	//deprecated
    ArchiveWriter(unsigned char * outBuffer, size_t* size, const eFormats& format, const eCompressions& compression);
    ArchiveWriter(const eFormats& format, const eCompressions& compression);
    ~ArchiveWriter();

	// Compress all files in the list
	// If you just want to keep part of path in the compressed package. you need set 'basePath'.
	// eg, if the file path is test1/test2/test3.md, you can set basePath="test1/test2",
	// then the compressed package only keep 'test3.md' in the package.
    bool compress(const std::string& basePath, std::vector<std::string>& fileList);

	// open one archive file
	// note: if the path contains relative path, you need make the folder exists by yourself.
	// eg, test1/test2/test3.md, you need make sure 'test1' and 'test2' already created!
    bool open(const std::string& archiveFileName);

	// open buffer for file content to write
	bool open(std::list<unsigned char>& outBuffer);
	bool open(unsigned char * outBuffer, size_t* size);

	// set archive_write_set_format_option 
	// eg. SetFormatOption(FORMAT_COMPRESSION_7ZIP, FORMAT_COMPRESSION_7ZIP_STORE);
    bool setFormatOption(const std::string& option, const std::string& value);

	// add one file to archive file, if you just wanto keep the file name in the compressed package
	// you need input the 'entryName'
	// eg. filePath="test1/test2/test3.md", entryName="test3.md"
	// filePath - file full path
	// entryName - file name
	bool addFile(const std::string& filePath, const std::string& entryName = "");

	// add one new file with the data to archive file
	// entryName - file name, the file no need exists
	// data - file data
	// size - file size
	bool addFile(const std::string& entryName, const unsigned char * data, uint64_t size);

	// add directory to archive file
	// directoryName - directory full path, please make sure the directory exists
    bool addDirectory(const std::string& directoryName);

	// close the archive file.
	// this option will also called in ~ArchiveWriter(). but you'd better call manually when you opened the archive file.
	bool close();

	// this api only works if return false or exception happened,
	// return latest error code 
	int32_t getLastErrorCode() const;

	// this api only works if return false or exception happened,
	// return latest error message 
	std::string getLastError() const;

private:
    void checkError(const int32_t errCode, const bool closeBeforThrow = false);
    void addHeader(const std::string& entryName, const eFileTypes entryType,
        const long long size = 0, const int32_t permission = 0644);
    void addHeader(const std::string& filePath, const std::string& entryName = "");
    void addContent(const char byte);
    void addContent(const char* bytes, uint64_t size);
	void addFinish();

	bool m_open;
	archive* m_archive;
	archive_entry* m_entry;

    const std::string m_archiveFileName;
    const eFormats m_format;
	const eCompressions m_compression;
	std::string m_err;
	int32_t m_code;
};


#endif

